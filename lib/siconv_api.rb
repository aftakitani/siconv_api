require "rubygems"
require 'hashie'
#require 'hashie/dash'
#require 'hashie/hash_extensions'
#require 'hashie/ext'
require 'faraday'
require 'faraday_middleware'
require 'faraday_middleware/response/rashify'
require "siconv_api/version"
require "siconv_api/municipio"
require "siconv_api/orgao"
require "siconv_api/natureza_juridica"
require "siconv_api/programa"
require "siconv_api/programa_serializer"
require 'dotenv'
require 'logger'
begin
  require 'pry-byebug'
  require "awesome_print"
rescue LoadError
end

module SiconvApi
  def self.connect
    Faraday.new(url: 'http://api.convenios.gov.br/siconv/') do |faraday|
      faraday.request :url_encoded
      faraday.use FaradayMiddleware::Rashify
      faraday.response :rashify
      faraday.response :json
      # faraday.response :logger                  # log requests to STDOUT
      faraday.adapter Faraday.default_adapter
    end
  end
end
