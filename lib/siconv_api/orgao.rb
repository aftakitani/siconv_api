module SiconvApi
  class Orgao
    def self.find_by_id(id)
      response = SiconvApi.connect.get "dados/orgao/#{id}.json"
      response.body.orgaos.first
    rescue
      []
    end
  end
end
