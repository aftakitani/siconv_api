module SiconvApi
  module Serializers

    class Programa < Hash

      def initialize(programa=nil)
        self[:codigo] = programa.cod_programa_siconv
        orgao = SiconvApi::Orgao.find_by_id(programa.orgao_superior.orgao.id)
        self[:orgao] = {nome: "#{orgao.id} - #{orgao.nome}"} unless orgao.empty?
        orgao_vinculado = SiconvApi::Orgao.find_by_id(programa.orgao_vinculado.orgao.id)
        self[:orgao_vinculado] = {nome:"#{orgao_vinculado.id} - #{orgao_vinculado.nome}"} unless orgao_vinculado.empty?
        orgao_executor = SiconvApi::Orgao.find_by_id(programa.orgao_executor.orgao.id)
        self[:orgao_executor] = {nome: "#{orgao_executor.id} - #{orgao_executor.nome}"} unless orgao_executor.empty?
        self[:nome] = programa.nome
        self[:descricao] = programa.descricao
        self[:estados] = programa.ufs_habilitadas
        atende_a = SiconvApi::NaturezaJuridica.find_by_id(programa.atende_a.first.natureza_juridica.id) if programa.atende_a && !programa.atende_a.empty?
        self[:atende_a] = atende_a.nome if atende_a && !atende_a.empty?
        self[:data_inicio_recebimento] = programa.data_inicio_recebimento_propostas
        self[:data_fim_recebimento] = programa.data_fim_recebimento_propostas
        self[:acao_orcamentaria] = programa.acao_orcamentaria
        self[:situacao] = programa.situacao      
      end
    end
  end
end

# SiconvApi::Serializers::Programa.new SiconvApi::Programa.find_by_id(74_219)
