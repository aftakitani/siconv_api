module SiconvApi
  class Programa

    def self.result_count
      response = SiconvApi.connect.get 'v1/consulta/programas.json', offset: 999_999_99
      response.body.metadados.total_registros
    end

    def self.find(page: -1)
      offset = nil
      pages = result_count / 500
      if page == -1 || page > pages
        offset = pages * 500
      else
        offset = page * 500
      end
      response = SiconvApi.connect.get 'v1/consulta/programas.json', offset: offset

      response.body.programas

    end

    def self.find_by_id(id)
      response = SiconvApi.connect.get "dados/programa/#{id}.json"
      response.body.programas.first
    end
  end
end
