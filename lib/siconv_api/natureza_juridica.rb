module SiconvApi
  class NaturezaJuridica
    def self.find_by_id(id)
      response = SiconvApi.connect.get "dados/natureza_juridica/#{id}.json"
      response.body.naturezajuridicas.first
    rescue
      []
    end
  end
end
