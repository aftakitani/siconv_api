module SiconvApi
  class Municipio
    def initialize
    end

    def self.connect
      Faraday.new(:url => 'http://api.convenios.gov.br/siconv/') do |faraday|
        faraday.request  :url_encoded             # form-encode POST params
        #faraday.response :mash
        faraday.use FaradayMiddleware::Rashify
        faraday.response :rashify
        faraday.response :json
        #faraday.response :logger                  # log requests to STDOUT
        faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
      end
    end

    def self.find(uf: nil, nome: nil)
      response = connect.get 'v1/consulta/municipios.json', uf: uf, nome: nome
      binding.pry
      response.body
    end

    def self.find_by_id(cod_siconv: nil)
      response = connect.get "dados/municipio/#{cod_siconv}.json"
      response.body
    end
  end
end
