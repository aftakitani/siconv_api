require 'spec_helper'

describe SiconvApi::NaturezaJuridica do

  describe ".fetch" do
    it "fetches by id" do
      natureza = subject.class.find_by_id(id: 30)      
      expect(natureza).not_to be_empty
    end
  end

end
