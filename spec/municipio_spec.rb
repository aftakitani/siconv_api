require 'spec_helper'

describe SiconvApi::Municipio do

  describe ".fetch" do
    it "fetches by uf" do
      #binding.pry
      municipios = subject.class.find(uf:"SP")
      expect(municipios).not_to be_empty
    end

    it "fetches by nome" do
      #
      municipios = subject.class.find(uf: "SP", nome:"Sao José")
      binding.pry
      expect(municipios).not_to be_empty
    end
  end

end
