require 'spec_helper'

describe SiconvApi::Programa do

  describe ".result_count" do
    it "gets the total results count" do
      expect(subject.class.result_count).to be_a Integer
    end
  end

  describe ".all" do
    xit "fetches all records" do
      programa = subject.class.find(page: -1)
      expect(programa).not_to be_empty
    end
  end

  describe ".fetch" do
    it "fetches by id" do
      programa = subject.class.find_by_id(id: 74_219)      
      expect(programa).not_to be_empty
    end

  end

end
